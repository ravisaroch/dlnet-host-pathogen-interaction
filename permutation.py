#Permutation of the particular Column
import numpy as np
import pandas as pd
import sys

df = pd.read_csv(sys.argv[1], header = None)

df.iloc[:,-1] = df.iloc[:,-1].sample (frac=1).reset_index (drop = True)
print(df)

df.to_csv("IN_forge_C", index=None, header=None)
#END
