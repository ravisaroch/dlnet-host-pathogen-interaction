## Pipeline for deep learning network construction of host pathogen interaction (DLNet-HPI) 

## Requirement

- Python3 (> v3.6) (https://www.python.org)
- Tensorflow (v2.4.1) (https://www.tensorflow.org)
- Scikit-learn (v0.24.1) (https://github.com/scikit-learn/scikit-learn)
- NumPy (v1.19.5) (https://numpy.org)
- Random Forest (Scikit-learn library)
- R  (>= v3.6) (https://cran.r-project.org)
- matplotlib (https://matplotlib.org)
- mcl (https://micans.org/mcl)

## Data formats
1. A comma seperated expression data profile matrix file without header with n rows (sample_number) and p columns (features_number), with and additional column to assign the class for a particular sample (0 or 1) (expression)
2. A gene file which includes the list of genes used in expresstion profile (Genes)
3. Gene Interaction file (Interaction)

## Run the script for 10 fold CV and significant modules extraction

- Final features score and true positive rate (TPR) against false positive rate (FPR) calculated with 10 fold cross validation impeded in custom shell (dlnet.sh) script
- Open the terminal, change the directory to the folder under which script.sh and all other files are located (or path can also be given to file location), then type:

```
chmod 755 dlnet.sh
./dlnet.sh expression RF Genes
```
It generates the output files in the same working directory, includes:

- Avg_feature_Imp - features importance score 
- AUC_FPR_TPR - True positive rate with respect to their false positive rate (Classification performance)
- MCL_modules - Total number of the modules for given interation (MCL algorithms)
- Significant_modules - List of sgnificant modules with their respective gene members
- Sig_Module_Gene - List of the genes participate in the significant modules
- ROC.png - ROC-AUC Curve generated saved

## Parameters
- Hyperparameter and model training option, available in python script (dlnet_Rank.py). Number of CV option and numbers of background enrichment score for significant module extraction, available in dlnet.sh script 

## Computational Cost
- Approximate time  90 min for 100 samples, ~4000 features and ~40000 interactions with 5000 decision trees.
- 256 GB RAM and Intel® Xeon(R) Silver 4114 CPU @ 2.20GHz × 40 processors.

## Publication
Kumar R, Khatri A, Acharya V (2022)
Deep learning uncovers distinct behavior of rice network to pathogens response.iScience 25(7), 104546. https://doi.org/10.1016/j.isci.2022.104546 



## If you have any questions, bug reports, or suggestions, please e-mail

Ravi Kumar (ravisaroch@gmail.com); Abhshek Khatri (khatriabhi2319@gmail.com); Vishal Acharya (vishal@ihbt.res.in)

Institute of Himalayan Bioresource Technology (CSIR-IHBT), Palampur

