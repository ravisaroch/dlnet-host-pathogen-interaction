#!/bin/bash

read -p "Please Enter your interaction data file : "  Interaction 


if [ -f $1 ]
then
	for i in {1..10};
	do
		echo "python dlnet_Rank.py $1 $2 && mv $1_forgeImp $1_forgeImp_$i && mv AUC_FPR AUC_FPR_$i && mv AUC_TPR AUC_TPR_$i"; 
	done  | sh
else
	echo "$(tput setaf 9) input file is not available in your present directory"
	exit 1
fi




for i in *forgeImp*
do
	echo $i | awk 'ORS=" "' 
done | sed "s/^/paste $3 /g" |sh  >  SCORE_forgeImp


for i in AUC_FPR_*
do
        echo $i | awk 'ORS=" "'
done | sed "s/^/paste /g" |sh  >  SCORE_AUC_FPR



for i in AUC_TPR*
do
        echo $i | awk 'ORS=" "'
done | sed "s/^/paste /g" |sh  >  SCORE_AUC_TPR

for i in $1_forgeImp*
do 
	echo $i | awk 'ORS=" "'
done | sed 's/^/rm /g' |sh 



for i in AUC_*
do
        echo $i | awk 'ORS=" "'
done | sed "s/^/rm /g" |sh


####################################### Average feature importance  of SCORE_forgeImp #################

if [ -f SCORE_forgeImp ]
then
	awk -F"\t" '{sum = 0; for (i = 2; i <= NF; i++) sum += $i; sum /= NF-1; print $0"\t"sum}' SCORE_forgeImp > Mean_SCORE_forgeImp
	awk -F "\t" '{print $1,$NF}' Mean_SCORE_forgeImp > Avg_feature_Imp
else
	echo "$(tput setaf 9) SCORE_forgeImp file not found"
	exit 1
fi

######################################################### FPR


if [ -f SCORE_AUC_FPR ]
then
        awk -F"\t" '{sum = 0; for (i = 1; i <= NF; i++) sum += $i; sum /= NF; print sum}' SCORE_AUC_FPR | awk -F"\t" '{print $NF}' > mean_FPR
        sed -i '1 i\0' mean_FPR 
else
        echo "$(tput setaf 9) SCORE_forgeImp file not found"
        exit 1
fi

##########################  TPR

if [ -f SCORE_AUC_TPR ]
then
        awk -F"\t" '{sum = 0; for (i = 1; i <= NF; i++) sum += $i; sum /= NF; print sum}' SCORE_AUC_TPR | awk -F"\t" '{print $NF}' > mean_TPR
        sed -i '1 i\0' mean_TPR 
else
        echo "$(tput setaf 9) SCORE_forgeImp file not found"
        exit 1
fi

############################### combine FPR & TPR

if [[ -f mean_FPR && -f  mean_TPR ]]
then
        paste mean_FPR mean_TPR | sed '1 i\FPR TPR' | sed 's/\t/ /g' > FPR_TPR
else
        echo "mean_FPR & mean_TPR files not found"
fi


######################## AUC_ROC plot for model classification ##########################

if [ -f mean_roc.py ]
then
	python mean_roc.py
	rm mean_FPR mean_TPR
	echo "$(tput setaf 10) featues importance and model classification Successfully done "
else 
	echo "$(tput setaf 9) mean_roc.py file not found"	
        exit 1
fi



############################################### significant  Network modules extraction 

if [[ -f Avg_feature_Imp ]]
then
	sort -nk 2 Avg_feature_Imp > Decreasing_order
else
	echo "$(tput setaf 9) Error: Avg_feature_Imp not found"
	exit 1
fi


if [[ -f $Interaction ]]
then
	sed 's/$/ 1/g' $Interaction > $Interaction\_mcl

	echo "Modules Construction progress............."
	mcxload -abc Interaction_mcl --stream-mirror -write-tab one.tab -o one.mci
	mcl one.mci -I 2
	mcxdump -icl out.one.mci.I20 -tabr one.tab -o dump.one.mci.I20


	#filtering module having 3 or more genes
	echo "Retaining cluster having 3 or more gene......."
	awk -F'\t' 'NF>=3{print}{}' dump.one.mci.I20 | sed -e "s/[[:space:]]\+/ /g" > MCL_module
	rm one.mci out.one.mci.I20 one.tab dump.one.mci.I20

network() {



#################################### function network.sh FOR CALCULATION ES value 

echo "Calculating Enrichment score for each module......"
cp MCL_module MCL_module_new && awk '{print$1}' Decreasing_order > temp && paste temp Decreasing_order | sed 's/\t/\//' |sed  "s/.*/\sed -i '\s\/&\/g' MCL_module_new /g" | sh && rm temp


#calculate the minimum and maximum gene score of each module and ES score
awk '{for (i=2;i<=NF;i+=2) {$2=($i<$2)?$i:$2} print $2}' MCL_module_new > min &&  awk '{for (i=2;i<=NF;i+=2) {$2=($i>$2)?$i:$2} print $2}' MCL_module_new > max && paste min max > min_max && rm min max MCL_module_new

sed "s/.*/awk '\{\if \(\$2 \>= &\) \print\$1\}' Decreasing_order \> RangeGene_M/g" min_max | sed 's/\t/ \&\& \$2 <= /g' | awk '{print $0 NR }' | sh
for i in  RangeGene_M*; do echo $i; done | sed 's/_/ /g' | awk '{print$1}' | awk '{print $0 "_M"NR}' | awk 'ORS=" " ' | sed 's/^/paste /g' | sed 's/$/ \> All_range/g' | sh && awk -F'\t' '{ for (i=1; i<=NF; i++) RtoC[i]= (RtoC[i]? RtoC[i] FS $i: $i) } END{ for (i in RtoC) print RtoC[i] }' All_range | sed -e "s/[[:space:]]\+/ /g" > temp && mv temp All_range && rm RangeGene*

#echo "awk '(FNR==NR){\$1=\$1; a[FNR]=OFS \$0 OFS; next} f && (FNR==1) { print m,nr-m } (FNR==1){f++; nr=m=0} {nr++; if(a[f] ~ OFS \$1 OFS) m++ } END { print m,nr-m }' MCL_module" > temp1 && for i in  RangeGene_M*; do echo $i; done | sed 's/_/ /g' | awk '{print$1}' | awk '{print $0 "_M"NR}' | awk 'ORS=" " ' | sed 's/$/ > match_mismatch/g' > temp2 && paste temp1 temp2 | sh && awk '{print $1}' && rm temp1 temp2 RangeGene_M*


awk -v N="$(wc -l < Decreasing_order)" '{for (i=1;i<=NF;i++) {arr[$i]}; M=NF; getline<"All_range"; for (i=1; i<=NF; i++){if ($i in arr){printf "%f ", sqrt((N-M)/M)} else{printf "%f ", -sqrt(M/(N-M))}}; delete arr; print ""}' MCL_module | awk '{ sum=0; for (i=1; i<=NF; ++i) {sum+=$i; $i=sum }; print $0}' | awk '{m=$1;for(i=1;i<=NF;i++)if($i>m)m=$i;print m}' > ES_score && rm All_range min_max Decreasing_order
echo "Enrichment score calculated and save in file ES_score"



}

#################################### function end network.sh FOR CALCULATION ES value 

network 

mv ES_score ES_original
echo "$(tput setaf 10)Enrichment score calculated successfully"
echo "$(tput setaf 10)The original Enriched score stored in 'ES_original' file "

else
        echo "$(tput setaf 9) Error: please check MCL Installation or/and  Interaction file in present directory "

fi

############################### Extract significant Module

for i in {1..100};
do
        python permutation.py expression && python dlnet_Rank.py IN_forge_C RF && paste Genes IN_forge_C_forgeImp | sort -r -nk2 | sed 's/\t/ /g'> Decreasing_order && network && mv ES_score ES_rand_$i
done


paste ES_rand* | sed 's/\t/ /g' > All_random_ES
rm ES_rand*



################################ T-test

sed -e "s/[[:space:]]\+/ /g"  All_random_ES | sed 's/ /, /g' | sed 's/.*/a =c \(&\)/g'  > script_100 && sed 's/.*/t.test \(a, mu\= &\)/g' ES_original > script_original && awk '{print; getline < "script_original"; print}' script_100 > script_t.test.R && rm script_100 script_original && Rscript script_t.test.R > output_t_test

grep "t =" output_t_test | awk '{print $NF}' >  output_test
grep "t = " output_t_test | sed 's/,.*//' | sed 's/.*\ //g' >  output_test_1

paste output_test output_test_1 MCL_module | sed -e "s/[[:space:]]\+/ /g"  | awk '{print "M"NR,$0}' | sed -e "s/[[:space:]]\+/ /g" | awk '$2 <= 0.05 && $3 > 0'  > Significant_module && rm output_* script_t.test.R AUC_* All_*  ES_original IN_forge_C  IN_forge_C_forgeImp Interaction_mcl Mean_SCORE_forgeImp SCORE_*
awk '!($1=$2=$3="")' Significant_module | sed 's/^  //g'| awk '{ for (i=1; i<=NF; i++) print $i }' > Sig_Module_Gene












