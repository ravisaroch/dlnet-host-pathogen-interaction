import numpy
from matplotlib import pyplot
lw = 1
mean_fpr= numpy.loadtxt("mean_FPR")
mean_tpr= numpy.loadtxt("mean_TPR")
print(mean_fpr)
print(mean_tpr)

pyplot.plot(mean_fpr, mean_tpr, lw=lw, linestyle='--', color='red', label='ROC_AUC')
pyplot.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')

#for i,j in zip(mean_fpr,mean_tpr):
   #pyplot.annotate(str(j),xy=(i,j))

#axis labels
pyplot.xlabel('False Positive Rate')
pyplot.ylabel('True Positive Rate')


#show the legend
pyplot.grid(linestyle='--')
pyplot.legend(loc="lower right")
  

#show the plot
pyplot.savefig('ROC', dpi = 500)
#pyplot.show()
#
